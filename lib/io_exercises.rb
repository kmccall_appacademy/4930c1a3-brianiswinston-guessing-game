# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  num = rand(1..100)
  num_guesses = 0
  loop do
    p 'Guess a number, please?'
    guess = gets.chomp.to_i
    num_guesses += 1
    p "You guessed #{guess}."
    if guess > num
      p 'Your guess is too high!'
    elsif guess < num
      p 'Your guess is too low!'
    elsif guess == num
      p 'You got it right!'
      break
    end
  end
  p "The answer was #{num}."
  p "And it took you #{num_guesses} to get it right. Good job!"
end

if __FILE__ == $PROGRAM_NAME
  puts "Please provide a filename!"
  file1 = gets.chomp
  file2 = File.open("shuffle", 'w')
  File.open(file1).each do |line|
    shuffled = line.chars.shuffle.join
    file2.write(shuffled)
  end
  file2.close
end
